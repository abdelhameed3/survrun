/*global $ */

//document.cookie
//document.cookie("name=value; expires=Date; path=") //set//

/*Parallax
=====================*/

$(document).ready(function () {
    
    "use strict";
    
    var $window = $(window);
    
    $('[data-type="background"]').each(function () {
        
        var $bgobj = $(this);
        
        $(window).scroll(function () {
            
            var ypos = -($window.scrollTop() / $bgobj.data('speed')),
                
                coords = '100%' + ypos + 'px';
            
            $bgobj.css({
                
                backgroundPosition: coords
                
            });
            
        });
        
    });
    
});
/* Slider Reviews
========================== */
$(document).ready(function () {
    "use strict";
    $(".reviews .slider").slick({
        prevArrow: ".reviews .next",
        nextArrow: ".reviews .prev",
        slidesToShow: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        dots: false,
        infinite: true,
        speed: 500
    });
});